package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Baralho {
    private List<Carta> baralho = new ArrayList<>();

    public void iniciarBaralho(Baralho baralho){
        for(EnumNumero numero: EnumNumero.values()) {
            for (EnumNaipe naipe:EnumNaipe.values()) {
                Carta carta = new Carta(numero,naipe);
                this.baralho.add(carta);
            }
        }
    }

    public List<Carta> getBaralho() {
        return baralho;
    }

    public void setBaralho(List<Carta> baralho) {
        this.baralho = baralho;
    }
}
