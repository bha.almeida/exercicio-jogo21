package br.com.itau;

import java.util.Scanner;

public class IO {
    public static int MenuJogo() {
        int opcao = 0;
        System.out.println("Selecione a opção do jogo: ");
        System.out.println("1 - comprar carta");
        System.out.println("2 - mostrar histórico de jogos");
        System.out.println("3 - sair do jogo");
        Scanner scanner = new Scanner(System.in);
        opcao = scanner.nextInt();
        return opcao;
    }

}
