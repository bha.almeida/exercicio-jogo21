package br.com.itau;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        boolean fimDeJogo = false;
        int opcao = 0;

        Jogo jogo = new Jogo();
        Baralho baralho = jogo.iniciaJogo();
        do {
            opcao = IO.MenuJogo();
            if (opcao == 1) {
                int total = jogo.comprarCarta(baralho);
                System.out.println(total);
                fimDeJogo = jogo.verifica21(total);
                if (fimDeJogo) {
                    break;
                }
            } else if (opcao == 2) {
                for (int i = 0; i < jogo.getHistorico().size(); i++) {
                    System.out.print(jogo.getHistorico().get(i));
                }
            }
        } while (opcao != 3);

    }
}
