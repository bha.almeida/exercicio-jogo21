package br.com.itau;

public class Carta {

    private EnumNumero numero;
    private EnumNaipe naipe;

    public Carta(EnumNumero numero, EnumNaipe naipe) {
        this.numero = numero;
        this.naipe = naipe;
    }

    public EnumNumero getNumero() {
        return numero;
    }

    public void setNumero(EnumNumero numero) {
        this.numero = numero;
    }

    public EnumNaipe getNaipe() {
        return naipe;
    }

    public void setNaipe(EnumNaipe naipe) {
        this.naipe = naipe;
    }
}
