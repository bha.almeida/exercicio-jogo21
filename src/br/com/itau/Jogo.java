package br.com.itau;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Jogo {
    Random random = new Random();
    Integer total = 0;
    private List<Integer> historico = new ArrayList<>();

    public int comprarCarta(Baralho baralho){
        int index = random.nextInt(52);
        Carta cartacomprada = baralho.getBaralho().get(index);
        System.out.println(cartacomprada.getNumero() + " de " + cartacomprada.getNaipe());
        baralho.getBaralho().remove(cartacomprada);
        somaValorCarta(cartacomprada);
        return total;
    }

    public int somaValorCarta(Carta carta){
        total = total + carta.getNumero().valorCarta;
        return total;
    }

    public boolean verifica21(int total){
        boolean fimDeJogo = false;
        if(total > 21){
            System.out.println("Você perdeu!");
            this.historico.add(total);
            fimDeJogo = true;
        } else if(total == 21){
            System.out.println("Você ganhou!");
            this.historico.add(total);
            fimDeJogo = true;
        }
        return fimDeJogo;
    }

    public Baralho iniciaJogo(){
        Baralho baralho = new Baralho();
        baralho.iniciarBaralho(baralho);
        return baralho;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<Integer> getHistorico() {
        return historico;
    }

    public void setHitorico(List<Integer> historico) {
        this.historico = historico;
    }
}
